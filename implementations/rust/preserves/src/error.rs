use num::bigint::BigInt;
use std::convert::From;
use std::io;

#[derive(Debug)]
pub enum Error {
    Io(io::Error),
    Message(String),
    InvalidUnicodeScalar(u32),
    NumberOutOfRange(BigInt),
    CannotDeserializeAny,
    MissingCloseDelimiter,
    MissingItem,
    Expected(ExpectedKind, Received),
    StreamingSerializationUnsupported,
}

#[derive(Debug)]
pub enum Received {
    ReceivedSomethingElse,
    ReceivedRecordWithLabel(String),
    ReceivedOtherValue(String),
}

#[derive(Debug, PartialEq)]
pub enum ExpectedKind {
    Boolean,
    Float,
    Double,

    SignedIntegerI128,
    SignedIntegerU128,
    SignedInteger,
    String,
    ByteString,
    Symbol,

    Record(Option<usize>),
    SimpleRecord(String, Option<usize>),
    Sequence,
    Set,
    Dictionary,

    Embedded,

    SequenceOrSet, // Because of hacking up serde's data model: see open_sequence_or_set etc.

    Option,
    UnicodeScalar,
}

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Self {
        Error::Io(e)
    }
}

impl From<Error> for io::Error {
    fn from(e: Error) -> Self {
        match e {
            Error::Io(ioe) => ioe,
            Error::Message(str) => io::Error::new(io::ErrorKind::Other, str),
            _ => io::Error::new(io::ErrorKind::Other, e.to_string()),
        }
    }
}

impl serde::ser::Error for Error {
    fn custom<T: std::fmt::Display>(msg: T) -> Self {
        Self::Message(msg.to_string())
    }
}

impl serde::de::Error for Error {
    fn custom<T: std::fmt::Display>(msg: T) -> Self {
        Self::Message(msg.to_string())
    }
}

impl std::error::Error for Error {}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

//---------------------------------------------------------------------------

pub fn is_io_error(e: &Error) -> bool {
    matches!(e, Error::Io(_))
}

pub fn eof() -> Error {
    Error::Io(io_eof())
}

pub fn is_eof_error(e: &Error) -> bool {
    if let Error::Io(ioe) = e {
        is_eof_io_error(ioe)
    } else {
        false
    }
}

pub fn syntax_error(s: &str) -> Error {
    Error::Io(io_syntax_error(s))
}

pub fn is_syntax_error(e: &Error) -> bool {
    if let Error::Io(ioe) = e {
        is_syntax_io_error(ioe)
    } else {
        false
    }
}

//---------------------------------------------------------------------------

pub fn io_eof() -> io::Error {
    io::Error::new(io::ErrorKind::UnexpectedEof, "EOF")
}

pub fn is_eof_io_error(e: &io::Error) -> bool {
    matches!(e.kind(), io::ErrorKind::UnexpectedEof)
}

pub fn io_syntax_error(s: &str) -> io::Error {
    io::Error::new(io::ErrorKind::InvalidData, s)
}

pub fn is_syntax_io_error(e: &io::Error) -> bool {
    matches!(e.kind(), io::ErrorKind::InvalidData)
}
