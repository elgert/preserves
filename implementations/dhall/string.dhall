{-|
Create a Preserves string from a `Text` value
-}
let Preserves/Type = ./Type.dhall

let Preserves/function = ./function.dhall

let string
    : Text → Preserves/Type
    = λ(x : Text) →
      λ(Preserves : Type) →
      λ(value : Preserves/function Preserves) →
        value.string x

in  string
