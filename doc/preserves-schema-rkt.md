---
title: preserves-schema-rkt
---

The `preserves-schema-rkt` program reads
[Preserves Schema](../preserves-schema.html) DSL input files. For each
input file, it produces a Racket source file of the same name but
with `.rkt` in place of `.prs`.

Instead of using this tool, you may prefer to use `#lang
preserves-schema` to use Schema DSL syntax in an ordinary Racket
module source file.

## Installation

Install Racket. Then, `raco pkg install preserves`.

## Usage

    usage: preserves-schema-rkt [ <option> ... ] [<input-glob>] ...

    <option> is one of

      --output <directory>
         Output directory for modules (default: next to sources)
      --stdout
         Prints each module to stdout one after the other instead of writing them to files in the `--output` directory
      --no-write-files
         Disables generation of output to the filesystem
      --base <directory>
         Base directory for sources (default: common prefix)
    * --module <namespace=path>
         Additional Namespace=path import
    * --plugin-lib <lib-path>, -l <lib-path>
         Load compiler plugin library
    * --plugin-file <rkt-file-path>, -f <rkt-file-path>
         Load compiler plugin source file
      --help, -h
         Show this help
      --
         Do not treat any remaining argument as a switch (at this level)

     *   Asterisks indicate options allowed multiple times.

     Multiple single-letter switches can be combined after
     one `-`. For example, `-h-` is the same as `-h --`.
