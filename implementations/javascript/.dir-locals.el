((typescript-mode
  . ((eval
      . (progn
          ;; For TIDE:
           (setq tide-tsserver-executable
                   (concat
                    (let ((d (dir-locals-find-file ".")))
                      (if (stringp d) d (car d)))
                    "node_modules/typescript/lib/tsserver.js"))
           ;; For LSP:
           (require 'lsp-javascript)
           (let ((node-modules (concat
                                (let ((d (dir-locals-find-file ".")))
                                  (if (stringp d) d (car d)))
                                "node_modules/")))
             (lsp-dependency 'typescript-language-server
                             `(:system ,(concat node-modules
                                                "typescript-language-server/lib/cli.mjs")))
             (lsp-dependency 'typescript
                             `(:system ,(concat node-modules
                                                "typescript/lib/tsserver.js")))))
      ))))
