import { readSchema, Meta } from '../src/index';

describe('reader schema', () => {
    it('complains about bad version', () => {
        expect(() => readSchema('version 999 .')).toThrow(/Invalid Version/);
    });
    it('complains about missing version', () => {
        expect(() => readSchema('')).toThrow(/missing version/);
    });
    it('is OK with an empty schema correctly versioned', () => {
        const s = readSchema('version 1 .');
        expect(Object.getOwnPropertyNames(s.version)).toEqual(['__as_preserve__']);
        expect(s.definitions.size).toBe(0);
        expect(s.embeddedType._variant).toBe('false');
    });
    it('understands patterns under embed', () => {
        const s = readSchema('version 1 . X = #!0 .');
        const def: Meta.Definition = s.definitions.get(Symbol.for('X'))!;
        if (def._variant !== 'Pattern') fail('bad definition 1');
        if (def.value._variant !== 'SimplePattern') fail ('bad definition 2');
        if (def.value.value._variant !== 'embedded') fail('bad definition 3');
        const i = def.value.value.interface;
        if (i._variant !== 'lit') fail('Non-tuple embedded pattern');
        expect(i.value).toBe(0);
    });
});
